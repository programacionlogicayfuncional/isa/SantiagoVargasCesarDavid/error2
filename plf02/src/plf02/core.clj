(ns plf02.core)

(defn funcion-associative?-1
  [a]
  (associative? [a a]))

(defn funcion-associative?-2
  [a b]
  (associative? '(a b)))

(defn funcion-associative?-3
  [a b c]
 (associative? {:a a :b b :c c}))

(funcion-associative?-1 1)
(funcion-associative?-2 2 3)
(funcion-associative?-3 4 5 6)


(defn funcion-boolean?-1
  [a]
  (boolean? a))

(defn funcion-boolean?-2
  [a]
  (boolean? (new Boolean a)))

(defn funcion-boolean?-3
  [a]
  (boolean? (new Boolean a)))

(funcion-boolean?-1 true)
(funcion-boolean?-2 "true")
(funcion-boolean?-3 "false")


(defn funcion-char?-1
  [a]
  (char? a))

(defn funcion-char?-2
  [a]
  (char? a))

(defn funcion-char?-3
  [a]
  (char? (first a)))

(funcion-char?-1 \a)
(funcion-char?-2 22)
(funcion-char?-3 "abc")


(defn funcion-coll?-1
  [a]
  (coll? a))

(defn funcion-coll?-2
  [a]
  (coll? (seq a)))

(defn funcion-coll?-3
  [a]
  (coll? a))

(funcion-coll?-1 4)
(funcion-coll?-2 "fred")
(funcion-coll?-3 true)


(defn funcion-decimal?-1
  [a]
  (decimal? a))

(defn funcion-decimal?-2
  [a]
  (decimal? a))

(defn funcion-decimal?-3
  [a]
  (decimal? a))

(funcion-decimal?-1 1)
(funcion-decimal?-2 1.0)
(funcion-decimal?-3 1M)


(defn funcion-double?-1
  [a]
  (double? a))

(defn funcion-double?-2
  [a]
  (double? (new Double a)))

(defn funcion-double?-3
  [a]
  (double? (new BigDecimal a)))

(funcion-double?-1 1.0)
(funcion-double?-2 "1")
(funcion-double?-3 "2")


(defn funcion-float?-1
  [a]
  (float? a))

(defn funcion-float?-2
  [a]
  (float? a))

(defn funcion-float?-3
  [a]
  (float? a))

(funcion-float?-1 0.0M)
(funcion-float?-2 0.0)
(funcion-float?-3 0)


(defn funcion-ident?-1
  [a]
  (ident? a))

(defn funcion-ident?-2
  [a]
  (ident? a))

(defn funcion-ident?-3
  [a]
  (ident? a))

(funcion-ident?-1 :hello)
(funcion-ident?-2 'abc)
(funcion-ident?-3 "hi")


(defn funcion-indexed?-1
  [a]
  (indexed? a))

(defn funcion-indexed?-2
  [a]
  (indexed? a))

(defn funcion-indexed?-3
  [a]
  (indexed? a))

(funcion-indexed?-1 1)
(funcion-indexed?-1 10)
(funcion-indexed?-1 "hola")


(defn funcion-int?-1
  [a]
  (int? a))

(defn funcion-int?-2
  [a]
  (int? (java.lang.Integer. a)))

(defn funcion-int?-3
  [a]
  (int? (java.lang.Long. a)))

(funcion-int?-1 42)
(funcion-int?-2 10)
(funcion-int?-3 8)


(defn funcion-integer?-1
  [a]
  (integer? a))

(defn funcion-integer?-2
  [a]
  (integer? a))

(defn funcion-integer?-3
  [a]
  (integer? (inc Integer/MAX_VALUE)))

(funcion-integer?-1 1)
(funcion-integer?-2 1.0)
(funcion-integer?-3 1)


(defn funcion-keyword?-1
  [a]
  (keyword? 'a))

(defn funcion-keyword?-2
  [a]
  (keyword? a))

(defn funcion-keyword?-3
  [a]
  (keyword? a))

(funcion-keyword?-1 1)
(funcion-keyword?-2 :x)
(funcion-keyword?-3 true)


(defn funcion-list?-1
  [a b c]
  (list? '(a b c)))

(defn funcion-list?-2
  [a b]
  (list? (list a b)))

(defn funcion-list?-3
  [a]
  (list? a))

(funcion-list?-1 1 2 3)
(funcion-list?-2 1 2)
(funcion-list?-3 0)


(defn funcion-map-entry?-1
  [a b]
  (map-entry? (first {:a a :b b})))

(defn funcion-map-entry?-2
  [a b c]
  (map-entry? (first {:a a :b b :c c})))

(defn funcion-map-entry?-3
  [a]
  (map-entry? (first {:a a})))

(funcion-map-entry?-1 1 2)
(funcion-map-entry?-2 1 8 9)
(funcion-map-entry?-3 1)


(defn funcion-map?-1
  [a b c]
  (map? {:a a :b b :c c}))

(defn funcion-map?-2
  [a b]
  (map? (hash-map :a a :b b)))

(defn funcion-map?-3
  [a b c]
  (map? (sorted-map :a a :b b :c c)))

(funcion-map?-1 1 4 5)
(funcion-map?-2 9 0)
(funcion-map?-3 1 4 5)

  
(defn funcion-nat-int?-1
  [a]
  (nat-int? a))

(defn funcion-nat-int?-2
  [a]
  (nat-int? (* 2 a)))

(defn funcion-nat-int?-3
  [a]
  (nat-int? (+ a a)))

(funcion-nat-int?-1 0)
(funcion-nat-int?-2 2036857)
(funcion-nat-int?-3 -1)


(defn funcion-number?-1
  [a]
  (number? a))

(defn funcion-number?-2
  [a]
  (number? a))

(defn funcion-number?-3
  [a]
  (number? a))

(funcion-number?-1 1)
(funcion-number?-1 :a)
(funcion-number?-1 "23")


(defn funcion-pos-int?-1
  [a]
  (pos-int? a))

(defn funcion-pos-int?-2
  [a]
  (pos-int? (* 3 a)))

(defn funcion-pos-int?-3
  [a b]
  (pos-int? (< a b)))

(funcion-pos-int?-1 9223372036854775807)
(funcion-pos-int?-2 20)
(funcion-pos-int?-3 2 3)


(defn funcion-ratio?-1
  [a]
  (ratio? a))

(defn funcion-ratio?-2
  [a b]
  (ratio? (* a b)))

(defn funcion-ratio?-3
  [a b]
  (ratio? (= a b)))

(funcion-ratio?-1 22/7)
(funcion-ratio?-2 2 3)
(funcion-ratio?-3 3 6)


(defn funcion-rational?-1
  [a]
  (rational? a))

(defn funcion-rational?-2
  [a b]
  (rational? (+ (* a b) b)))

(defn funcion-rational?-3
  [a b]
  (rational? (* a (+ a b))))

(funcion-rational?-1 4)
(funcion-rational?-2 2 4)
(funcion-rational?-3 7 7)


(defn funcion-seq?-1
  [a]
  (seq? a))

(defn funcion-seq?-2
  [a]
  (seq? [a]))

(defn funcion-seq?-3
  [a]
   (seq? (seq [(* 2 a)])))

(funcion-seq?-1 4)
(funcion-seq?-2 6)
(funcion-seq?-3 6)


(defn funcion-seqable?-1
  [a]
  (seqable? a))

(defn funcion-seqable?-2
  [a b]
  (seqable? (int-array a b)))

(defn funcion-seqable?-3
  [a]
  (seqable? (char-array a \c)))

(funcion-seqable?-1 nil)
(funcion-seqable?-2 1 3)
(funcion-seqable?-3 5)


(defn funcion-sequential?-1
  [a]
  (sequential? '(a a a)))

(defn funcion-sequential?-2
  [a b]
  (sequential? [a (* 2 a) (* 5 b)]))

(defn funcion-sequential?-3
  [a b c]
  (sequential? (range a (* b c))))

(funcion-sequential?-1 3)
(funcion-sequential?-2 3 5)
(funcion-sequential?-3 3 7 6)


(defn funcion-set?-1
  [a]
  (set? #{(* 3 a) (- a 9)}))

(defn funcion-set?-2
  [a b c]
  (set? (hash-set a b c)))

(defn funcion-set?-3
  [a b]
  (set? #{(* 3 a) (- a b)}))

(funcion-set?-1 3)
(funcion-set?-2 5 6 8)
(funcion-set?-3 9 6)


(defn funcion-some?-1
  [a]
  (some? a))

(defn funcion-some?-2
  [a b]
  (some? (< a b)))

(defn funcion-some?-3
  [a]
  (some? (not (< a (* 3 a)))))

(funcion-some?-1 8)
(funcion-some?-2 3 4)
(funcion-some?-3 5)


(defn funcion-string?-1
  [a]
  (string? a))

(defn funcion-string?-2
  [a b c]
  (string? [a b c]))

(defn funcion-string?-3
  [a b]
  (string? (+ a b)))

(funcion-string?-1 "hola")
(funcion-string?-2 "hola" "compa" "este")
(funcion-string?-3 5 4)


(defn funcion-symbol?-1
  [a]
  (symbol? 'a))

(defn funcion-symbol?-2
  [a]
  (symbol? (* 2 a)))

(defn funcion-symbol?-3
  [a]
  (symbol? a))

(funcion-symbol?-1 4)
(funcion-symbol?-2 6)
(funcion-symbol?-3 :a)


(defn funcion-vector?-1
  [a]
  (vector? [a (* 3 a) (- a (* 2 a))]))

(defn funcion-vector?-2
  [a]
  (vector? '(1 2 3)))

(defn funcion-vector?-3
  [a]
  (vector? (vec '(a a a))))

(funcion-vector?-1 5)
(funcion-vector?-2 6)
(funcion-vector?-3 7)


(defn funcion-drop-1
  [a]
   ((fn[x] (x(x (count (drop 1 [a 4 5]))))) (fn[y] (* 2 y))))

(defn funcion-drop-2
  [a b c]
   ((fn [x] (x (x (count (drop a [a b c]))))) (fn [y] (* 2 y))))

(defn funcion-drop-3
  [a]
  ((fn [x] (x (x (count (drop a [a (* 2 a) a a]))))) (fn [y] (* 2 y))))

(funcion-drop-1 1)
(funcion-drop-2 2 1 2)
(funcion-drop-3 30)


(defn funcion-drop-last-1
  [a]
  ((fn [x] (x (x (count (drop-last [a a]))))) (fn [y] (* 2 y))))

(defn funcion-drop-last-2
  [a b]
  ((fn [x] (x (x (count (drop-last [a (* a b)]))))) (fn [y] (* 2 y))))

(defn funcion-drop-last-3
  [a]
  ((fn [x] (x (x (count (drop-last [a (* 2 a)]))))) (fn [y] (* 2 y))))

(funcion-drop-last-1 3)
(funcion-drop-last-2 3 4)
(funcion-drop-last-3 3)


(defn funcion-drop-while-1
  [a]
 ((fn [x] (x (x (count (drop-while #(>= 3 %) [a a]))))) (fn [y] (* 2 y))))

(defn funcion-drop-while-2
  [a]
  ((fn [x] (x (x (count (drop-while #(>= 3 %) [a (* 2 a)]))))) (fn [y] (* 2 y))))

(defn funcion-drop-while-3
  [a]
  ((fn [x] (x (x (count (drop-while #(>= 3 %) [a (+ a a) (* a a)]))))) (fn [y] (* 2 y))))

(funcion-drop-while-1 4)
(funcion-drop-while-2 2)
(funcion-drop-while-3 1)


(defn funcion-every?-1
  [a]
  ((fn [x] (x (x (every? #{a 2} [a (* 2 a)])))) (fn [y] (true? y))))

(defn funcion-every?-2
  [a b]
  ((fn [x] (x (x (every? #{1 2} [a b])))) (fn [y] (true? y))))

(defn funcion-every?-3
  [a b c]
  ((fn [x] (x (x (every? #{1 2} [a b c])))) (fn [y] (true? y))))

(funcion-every?-1 5)
(funcion-every?-2 5 5)
(funcion-every?-3 5 7 4)


(defn funcion-filterv-1
  [a]
  ((fn [x] (x (x (count(filterv even? (range a)))))) (fn [y] (* 2 y))))

(defn funcion-filterv-2
  [a]
  ((fn [x] (x (x (count (filterv even? (range (* 2 a))))))) (fn [y] (* 2 y))))

(defn funcion-filterv-3
  [a b]
  ((fn [x] (x (x (count (filterv even? (range (+ a b))))))) (fn [y] (* 2 y))))

(funcion-filterv-1 5)
(funcion-filterv-2 5)
(funcion-filterv-3 5 7)


(defn funcion-group-by-1
  [a]
  ((fn [x] (x (x (count (group-by count [a "as" "asd" "aa" "asdf" "qwer"]))))) (fn [y] (* 2 y))))

(defn funcion-group-by-2
  [a b]
  ((fn [x] (x (x (count (group-by count [a "as" b "aa" "asdf" b]))))) (fn [y] (* 2 y))))

(defn funcion-group-by-3
  [a b c]
  ((fn [x] (x (x (count (group-by count [a "as" c "aa" b "qwer"]))))) (fn [y] (* 2 y))))

(funcion-group-by-1 "hola")
(funcion-group-by-2 "hoy" "adios")
(funcion-group-by-3 "fin" "inicio" "ok")


(defn funcion-iterate-1
  [a]
  ((fn [x] (x (x (count(take 5 (iterate inc a)))))) (fn [y] (* 2 y))))

(defn funcion-iterate-2
  [a]
  ((fn [x] (x (x (count(take a (iterate (partial + 2) 0)))))) (fn [y] (* 2 y))))

(defn funcion-iterate-3
  [a b]
  ((fn [x] (x (x (count (take b (iterate inc a)))))) (fn [y] (* 2 y))))

(funcion-iterate-1 5)
(funcion-iterate-2 2)
(funcion-iterate-3 2 3)


(defn funcion-keep-1
  [a]
  ((fn [x] (x (x (count(keep #(if (odd? %) %) (range a)))))) (fn [y] (* 2 y))))

(defn funcion-keep-2
  [a]
  ((fn [x] (x (x (count (keep #(if (odd? %) %) (range (* 2 a))))))) (fn [y] (* 2 y))))

(defn funcion-keep-3
  [a]
  ((fn [x] (x (x (count (keep #(if (odd? %) %) (range (* a a))))))) (fn [y] (* 2 y))))

(funcion-keep-1 5)
(funcion-keep-2 5)
(funcion-keep-3 5)


(defn funcion-keep-indexed-1
  [a]
 ((fn [x] (x (x (count(keep-indexed #(if (pos? %2) %1) [a a]))))) (fn [y] (* 2 y))))

(defn funcion-keep-indexed-2
  [a]
  ((fn [x] (x (x (count (keep-indexed #(if (pos? %2) %1) [a (* 2 a)]))))) (fn [y] (* 2 y))))

(defn funcion-keep-indexed-3
  [a]
  ((fn [x] (x (x (count (keep-indexed #(if (pos? %2) %1) [(* 3 a) (* a a) (* 2 a)]))))) (fn [y] (* 2 y))))

(funcion-keep-indexed-1 1)
(funcion-keep-indexed-2 2)
(funcion-keep-indexed-3 4)


(defn funcion-map-indexed-1
  [a]
  ((fn [x] (x (x (count(map-indexed (fn [idx itm] [idx itm]) a))))) (fn [y] (* 2 y))))

(defn funcion-map-indexed-2
  [a]
  ((fn [x] (x (x (count(map-indexed vector a))))) (fn [y] (* 2 y))))

(defn funcion-map-indexed-3
  [a]
  ((fn [x] (x (x (count (map-indexed (fn [idx itm] [idx itm]) a))))) (fn [y] (* 2 y))))

(funcion-map-indexed-1 "mira")
(funcion-map-indexed-2 "no se puede")
(funcion-map-indexed-3 "a mira no mas")


(defn funcion-mapcat-1
  [a b]
  ((fn [x] (x (x (count (mapcat reverse [[a a b] [b b b] [a (* a b)]]))))) (fn [y] (* 2 y))))

(defn funcion-mapcat-2
  [a b]
  ((fn [x] (x (x (count (mapcat reverse [[a b a] [a b b] [a (* a b)]]))))) (fn [y] (* 2 y))))

(defn funcion-mapcat-3
  [a b c]
  ((fn [x] (x (x (count (mapcat reverse [[a b c] [a (* b c)] [c c]]))))) (fn [y] (* 2 y))))

(funcion-mapcat-1 2 4)
(funcion-mapcat-2 5 6)
(funcion-mapcat-3 4 2 3)


(defn funcion-mapv-1
  [a]
  ((fn [x] (x (x (count(mapv inc [a a a a a]))))) (fn [y] (* 2 y))))

(defn funcion-mapv-2
  [a]
  ((fn [x] (x (x (count(mapv + [a (+ a a) a] [(* 2 a) a (* 3 a)]))))) (fn [y] (* 2 y))))

(defn funcion-mapv-3
  [a b]
  ((fn [x] (x (x (count (mapv inc [a b]))))) (fn [y] (* 2 y))))

(funcion-mapv-1 5)
(funcion-mapv-2 7)
(funcion-mapv-3 2 4)


(defn funcion-merge-with-1
  [a]
  ((fn [x] (x (x (count(merge-with + 
            {:a a  :b 2}
            {:a 9  :b 98 :c 0}))))) (fn [y] (* 2 y))))

(defn funcion-merge-with-2
  [a b]
  ((fn [x] (x (x (count(merge-with + 
            {:a 1  :b (* a b)}
            {:a (+ a b)  :b 98 :c 0}))))) (fn [y] (* 2 y))))

(defn funcion-merge-with-3
  [a b c]
  ((fn [x] (x (x (count (merge-with +
                                    {:a 1  :b c}
                                    {:a (+ a b)  :b 98 :c 0}))))) (fn [y] (* 2 y))))

(funcion-merge-with-1 7)
(funcion-merge-with-2 6 2)
(funcion-merge-with-3 6 3 1)


(defn funcion-not-any?-1
  [a]
  ((fn [x] (x (x a))) (fn [y] (not y))))

(defn funcion-not-any?-2
  [a b]
  ((fn [x] (x (x (true?(< a b))))) (fn [y] (not y))))

(defn funcion-not-any?-3
  [a b]
  ((fn [x] (x (x (not(= a b))))) (fn [y] (not y))))

(funcion-not-any?-1 true)
(funcion-not-any?-2 4 5)
(funcion-not-any?-3 1 2)


(defn funcion-not-every?-1
  [a]
  ((fn [x] (x (x (not-every? odd? '(1 2 3))))) (fn [y] (not y))))

(defn funcion-not-every?-2
  [a]
  ((fn [x] (x (x (not-every? odd? '(1 2 9 7 5))))) (fn [y] (not y))))

(defn funcion-not-every?-3
  [a]
  ((fn [x] (x (x (not-every? odd? '(2 3 4 5 6 7 1 1))))) (fn [y] (not y))))

(funcion-not-every?-1 1)
(funcion-not-every?-2 1)
(funcion-not-every?-3 4)


(defn funcion-partition-by-1
  [a]
  ((fn [x] (x (x (count(partition-by #(= 3 %) [a (* 2 a)]))))) (fn [y] (* 2 y))))

(defn funcion-partition-by-2
  [a b]
  ((fn [x] (x (x (count (partition-by #(= 3 %) [a (* a b)]))))) (fn [y] (* 2 y))))

(defn funcion-partition-by-3
  [a b c]
  ((fn [x] (x (x (count (partition-by #(= 3 %) [a (* 2 a) (* 3 (+ a c))]))))) (fn [y] (* 2 y))))

(funcion-partition-by-1 4)
(funcion-partition-by-2 3 4)
(funcion-partition-by-3 3 1 2)


(defn funcion-reduce-kv-1
  [a]
  ((fn [x] (x (x (count(reduce-kv #(assoc %1 %3 %2) {} {:a a}))))) (fn [y] (* 2 y))))

(defn funcion-reduce-kv-2
  [a b]
  ((fn [x] (x (x (count (reduce-kv #(assoc %1 %3 %2) {} {:a a :b (* 2 b)}))))) (fn [y] (* 2 y))))

(defn funcion-reduce-kv-3
  [a b c]
  ((fn [x] (x (x (count (reduce-kv #(assoc %1 %3 %2) {} {:b a :a c :c (* a b)}))))) (fn [y] (* 2 y))))

(funcion-reduce-kv-1 3)
(funcion-reduce-kv-2 4 5)
(funcion-reduce-kv-3 6 1 5)


(defn funcion-remove-1
  [a]
  ((fn [x] (x (x (count(remove pos? [a a]))))) (fn [y] (* 2 y))))

(defn funcion-remove-2
  [a b]
  ((fn [x] (x (x (count(remove pos? [a (* 2 b) (* a b)]))))) (fn [y] (* 2 y))))

(defn funcion-remove-3
  [a b c]
  ((fn [x] (x (x (count (remove pos? [a (+ a b) (- c a) a b]))))) (fn [y] (* 2 y))))

(funcion-remove-1 4)
(funcion-remove-2 3 2)
(funcion-remove-3 1 2 3)


(defn funcion-reverse-1
  [a]
  ((fn [x] (x (x (count(reverse '(a 2 3)))))) (fn [y] (* 2 y))))

(defn funcion-reverse-2
  [a]
  ((fn [x] (x (x (count (reverse a))))) (fn [y] (* 2 y))))

(defn funcion-reverse-3
  [a b c]
  ((fn [x] (x (x (count (reverse '(a b c)))))) (fn [y] (* 2 y))))

(funcion-reverse-1 5)
(funcion-reverse-2 "callar")
(funcion-reverse-3 3 5 8)


(defn funcion-some-1
  [a]
  ((fn [x] (x (x (some even? '(1 2 3))))) (fn [y] (true? y))))

(defn funcion-some-2
  [a]
  ((fn [x] (x (x (some true? [a a a])))) (fn [y] (nil? y))))

(defn funcion-some-3
  [a]
  ((fn [x] (x (x (some true? [a a a])))) (fn [y] (nil? y))))

(funcion-some-1 1)
(funcion-some-2 true)
(funcion-some-3 false)


(defn funcion-sort-by-1
  [a]
  ((fn [x] (x (x (count(sort-by first [[a a] [a a]]))))) (fn [y] (* 2 y))))

(defn funcion-sort-by-2
  [a]
  ((fn [x] (x (x (count(sort-by first > [[a (* 2 a)] [(* a a) a] ]))))) (fn [y] (* 2 y))))

(defn funcion-sort-by-3
  [a b c]
  ((fn [x] (x (x (count (sort-by first [[a (+ b c)] [c a] [b c]]))))) (fn [y] (* 2 y))))

(funcion-sort-by-1 5)
(funcion-sort-by-2 1)
(funcion-sort-by-3 2 5 6)

  
(defn funcion-split-with-1
  [a]
  (+ a)((fn [x] (x (x (count(split-with (partial >= 3) [a (* 2 a)]))))) (fn [y] (* 2 y))))

(defn funcion-split-with-2
  [a b]
  (+ a) ((fn [x] (x (x (count (split-with (partial > 3) [a (* b a)]))))) (fn [y] (* 2 y))))

(defn funcion-split-with-3
  [a b c]
  (+ a) ((fn [x] (x (x (count (split-with (partial > 10) [b (* 2 c) (+ a b)]))))) (fn [y] (* 2 y))))

(funcion-split-with-1 3)
(funcion-split-with-2 3 2)
(funcion-split-with-3 1 2 3)


(defn funcion-take-1
  [a]
  ((fn [x] (x (x (count(take 3 [a (* 2 a) (* 3 a)]))))) (fn [y] (* 2 y))))

(defn funcion-take-2
  [a b]
  ((fn [x] (x (x (count(take 3 [b (* 2 a) (* 3 b)]))))) (fn [y] (* 2 y))))

(defn funcion-take-3
  [a b c]
  ((fn [x] (x (x (count (take 3 [a (* c b) (* c a)]))))) (fn [y] (* 2 y))))

(funcion-take-1 3)
(funcion-take-2 2 8)
(funcion-take-3 1 5 7)


(defn funcion-take-last-1
  [a b c]
  ((fn [x] (x (x (count(take-last 2 [a b c]))))) (fn [y] (* 2 y))))

(defn funcion-take-last-2
  [a b c d e f]
  ((fn [x] (x (x (count (take-last 2 [a b c d e f]))))) (fn [y] (* 2 y))))

(defn funcion-take-last-3
  [a b c]
  ((fn [x] (x (x (count (take-last 2 [c a (+ a (* b (* 2 c)))]))))) (fn [y] (* 2 y))))

(funcion-take-last-1 3 1 2)
(funcion-take-last-2 3 2 4 5 6 7)
(funcion-take-last-3 1 9 0)


(defn funcion-take-nth-1
  [a]
  ((fn [x] (x (x (count(take-nth 2 (range a)))))) (fn [y] (* 2 y))))

(defn funcion-take-nth-2
  [a b]
  ((fn [x] (x (x (count (take-nth a (range b)))))) (fn [y] (* 2 y))))

(defn funcion-take-nth-3
  [a b c]
  ((fn [x] (x (x (count (take-nth a (range (* b c))))))) (fn [y] (* 2 y))))

(funcion-take-nth-1 2)
(funcion-take-nth-2 3 5)
(funcion-take-nth-3 1 2 3)


(defn funcion-take-while-1
  [a]
  ((fn [x] (x (x (reduce + (take-while (partial > a) (iterate inc 0)))))) (fn [y] (* 2 y))))

(defn funcion-take-while-2
  [a b]
  ((fn [x] (x (x (reduce + (take-while (partial > (* a b)) (iterate inc 0)))))) (fn [y] (* 2 y))))

(defn funcion-take-while-3
  [a b c]
  ((fn [x] (x (x (reduce + (take-while (partial > (* a (+ b c))) (iterate inc 0)))))) (fn [y] (* 2 y))))

(funcion-take-while-1 2)
(funcion-take-while-2 3 4)
(funcion-take-while-3 1 2 3)


(defn funcion-update-1
  [a]
  ((fn [x] (x (x (count(update [a a a] 0 inc))))) (fn [y] (* 2 y))))

(defn funcion-update-2
  [a b]
  ((fn [x] (x (x (count (update [(+ a b) (* a b)] 0 inc))))) (fn [y] (* 2 y))))

(defn funcion-update-3
  [a b c]
  ((fn [x] (x (x (count (update [(+ a b) (* b c)] 0 inc))))) (fn [y] (* 2 y))))

(funcion-update-1 1)
(funcion-update-2 1 2)
(funcion-update-3 7 4 6)


(defn funcion-update-in-1
  [a]
  ((fn [x] (x (x (count {:a a})))) (fn [y] (* 2 y))))

(defn funcion-update-in-2
  [a]
  ((fn [x] (x (x (count {:a a :b (+ a a)})))) (fn [y] (* 2 y))))

(defn funcion-update-in-3
  [a b]
  ((fn [x] (x (x (count {:a a :b (* a b)})))) (fn [y] (* 2 y))))

(funcion-filterv-1 6)
(funcion-filterv-2 1)
(funcion-filterv-3 9 1)

